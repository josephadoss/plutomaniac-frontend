import React, { Component } from 'react';
import {Panel} from 'primereact/panel';

export class AppFooter extends Component {


    render() {
        return  (
            <div>
                <br />
                <Panel header="Copyright Joseph Doss">
                    All rights reserved. All wrongs revenged. If you find this site informative, please share with your friends and enemies.
                </Panel> 
                <br />
                <br />
                If you're using this website for your own reasearch, please cite it and include a link to https://plutomaniac.com.
                <br />
                Thanks Cowboy!
            </div>
        );
    }

    ren2der() {
        return  (
            <div className="layout-footer">
                <span className="footer-text" style={{'marginRight': '5px'}}>PrimeReact</span>
                <img src="assets/layout/images/logo.svg" alt="" width="80"/>
                <span className="footer-text" style={{'marginLeft': '5px'}}>Theme and Layout</span>
            </div>
        );
    }
}