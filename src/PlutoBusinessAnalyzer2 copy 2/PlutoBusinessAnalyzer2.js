import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { Growl } from 'primereact/growl';
import { InputText } from 'primereact/inputtext';
import { Chart } from 'primereact/chart';
import {Column} from 'primereact/column';
import {DataTable} from 'primereact/datatable';
import {Messages} from 'primereact/messages';
import {Message} from 'primereact/message';
import { ProgressSpinner } from 'primereact/progressspinner';

import './node_modules/primereact/resources/themes/nova-light/theme.css';
import './node_modules/primereact/resources/primereact.min.css';
import './node_modules/primeicons/primeicons.css';

import './PlutoBusinessAnalyzer2.css';

class PlutoBusinessAnalyzer2 extends Component {
  constructor() {
    super();
    this.stockHistoryChartElement = React.createRef();

    var e = new Date();
    e.setDate(1);

    var b = new Date();
    b.setFullYear((e.getFullYear()-10));
    b.setDate(1);

    this.state = {
      ticker: 'XOM',
      tickerOptions: [
        { value: 'ABBV', label: 'ABBV - AbbVie' }
        , { value: 'ABT', label: 'ABT - Abbott Labs' }
        , { value: 'AEP', label: 'AEP - American Electric Power' }
        , { value: 'AXP', label: 'AXP - American Express' }
        , { value: 'BBL', label: 'BBL - BHP Group' }
        , { value: 'BDX', label: 'BDX - Becton Dickinson' }
        , { value: 'BP', label: 'BP - BP' }
        , { value: 'CAH', label: 'CAH - Cardinal Health' }
        , { value: 'CAT', label: 'CAT - Caterpillar' }
        , { value: 'CHD', label: 'CHD - Church & Dwight' }
        , { value: 'CL', label: 'CL - Colgate Palmolive' }
        , { value: 'CLX', label: 'CLX - Clorox' }
        , { value: 'COP', label: 'COP - ConocoPhillips' }
        , { value: 'CVX', label: 'CVX - Chevron Corporation' }
        , { value: 'DIS', label: 'DIS - The Walt Disney Company' }
        , { value: 'EMR', label: 'EMR - Emerson Electric Co.' }
        , { value: 'GE', label: 'GE - General Electric' }
        , { value: 'GIS', label: 'GIS - General Mills' }
        , { value: 'GSK', label: 'GSK - GlaxoSmithKline' }
        , { value: 'HSY', label: 'HSY - The Hershey Company' }
        , { value: 'JNJ', label: 'JNJ - Johnson & Johnson' }
        , { value: 'K', label: 'K - Kellogg\'s' }
        , { value: 'KO', label: 'KO - Coca Cola' }
        , { value: 'MA', label: 'MA - Mastercard' }
        , { value: 'MCD', label: 'MCD - McDonald\'s' }
        , { value: 'MKC', label: 'MKC - McCormick & Company' }
        , { value: 'MMM', label: 'MMM - 3M' }
        , { value: 'NSRGF', label: 'NSRGF - Nestlé' }
        , { value: 'NSRGY', label: 'NSRGY - Nestlé' }
        , { value: 'O', label: 'O - Realty Income' }
        , { value: 'PEP', label: 'PEP - PepsiCo' }
        , { value: 'PG', label: 'PG - Procter & Gamble' }
        , { value: 'PM', label: 'PM - Philip Morris International' }
        , { value: 'RDS.B', label: 'RDS.B - Royal Dutch Shell' }
        , { value: 'SJM', label: 'SJM - The J.M. Smucker Company' }
        , { value: 'SO', label: 'SO - Southern Company' }
        , { value: 'T', label: 'T - AT&T' }
        , { value: 'UL', label: 'UL - Unilever' }
        , { value: 'UN', label: 'UN - Unilever' }
        , { value: 'V', label: 'V - Visa' }
        , { value: 'WBA', label: 'WBA - Walgreens Boots Alliance' }
        , { value: 'WDFC', label: 'WDFC - WD-40 Company' }
        , { value: 'WTR', label: 'WTR - Aqua America' }
        , { value: 'XOM', label: 'XOM - Exxon' }
        , { value: 'YORW', label: 'YORW - The York Water Company' }
      ],
      showSpinner: true,
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      principal: [50, 25, 12, 48, 90, 76, 42],
      dividends: [21, 84, 24, 75, 37, 65, 34],
      reinvestedDividends: [1, 1, 1, 1, 1, 1, 1],
      reinvestedDividendsSum: 0.0,
      principalStart: 0.0,
      principalEnd: 0.0,
      principalEndReinvested: 0.0,
      dividendSum: 0.0,
      numberOfShares: 0.0,
      reinvestedNumberOfShares: 0.0,
      dateBegin: b,
      dateEnd: e,
      initialInvestment: 1000.0,
      cagr: 0.0,
      cagrReinvested: 0.0,
      monthlyInvestment:0.0,
      dividendYearlyStats:[],
      principalEndPlusDividendSum:0.0
    };
   
   this.reloadData(); 
   this.showApology = this.showApology.bind(this);
  }

  clearData() {
    var l = ['a'];
    var p = [0.0];
    var d = [0.0];
    this.setState({ labels: l });
    this.setState({ principal: p });
    this.setState({ dividends: d });
    this.setState({ reinvestedDividends: d });
    this.setState({ reinvestedDividendsSum: 0.0 });
    this.setState({ principalStart: 0.0 });
    this.setState({ principalEnd: 0.0 });
    this.setState({ dividendSum: 0.0 });
    this.setState({ dividendReinvestSum: 0.0 });
    this.setState({ numberOfShares: 0.0 });
    this.setState({ reinvestedNumberOfShares: 0.0 });
    this.setState({principalEndPlusDividendSum:0.0});
  }

  doClick(){
    this.reloadData();
  }


  reloadData() { 

    var initialInvestment = this.state.initialInvestment;
    var monthlyInvestment = this.state.monthlyInvestment;
    if (isNaN(initialInvestment)) {
      alert("Please enter a number for your initial investment.");
      return;
    }

    var ticker = this.state.ticker;
    var dateBegin = this.state.dateBegin.getFullYear() + "-" + (this.state.dateBegin.getMonth() + 1);
    var dateEnd = this.state.dateEnd.getFullYear() + "-" + (this.state.dateEnd.getMonth() + 1);

    var params = "?ticker=" + ticker + "&dateBegin=" + dateBegin + "&dateEnd=" + dateEnd + "&initialInvestment=" + initialInvestment+ "&monthlyInvestment=" + monthlyInvestment;
    var url = "./server/tickerData.php" + params;

    if (window.location.href.includes('localhost')) { url = "https://plutomaniac.com/server/tickerData.php" + params; }

    var comp = this;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        //alert(xhr.responseText);
        var json = JSON.parse(xhr.responseText); 

        comp.setState({ labels: json.labels });
        comp.setState({ principal: json.principal });
        comp.setState({ dividends: json.dividends });
        comp.setState({ reinvestedDividends: json.reinvestedDividends });
        comp.setState({ reinvestedDividendsSum: json.reinvestedDividendsSum });
        comp.setState({ principalStart: json.principalStart });
        comp.setState({ principalEnd: json.principalEnd });
        comp.setState({ principalEndPlusDividendSum: json.principalEndPlusDividendSum });
        comp.setState({ principalEndReinvested: json.principalEndReinvested });
        comp.setState({ dividendSum: json.dividendSum });
        comp.setState({ dividendReinvestSum: json.dividendReinvestSum });
        comp.setState({ shareCountInitial: json.shareCountInitial });
        comp.setState({ shareCountEnding: json.shareCountEnding });
        comp.setState({ shareCountReinvested: json.shareCountReinvested });
        comp.setState({ cagr: json.cagr });
        comp.setState({ cagrReinvested: json.cagrReinvested });
        comp.setState({ showSpinner: false });
        comp.setState({ dividendYearlyStats : json.dividendYearlyStats});
      }
    };
    xhr.open("GET", url, true);
    xhr.send(null);

    this.setState({ showSpinner: true });
  }

  showApology() { 
    // this.growl.show({ severity: 'warn', summary: 'Apology', detail: 'Sorry about the long wait.' }); 
   }

  render() { 
    var stackedData1 = {
      labels: this.state.labels,
      datasets: [{ type: 'bar', label: 'Total Dividends Received', backgroundColor: '#FFCA28', data: this.state.dividends },
      { type: 'bar', label: 'Shares Value', backgroundColor: '#66BB6A', data: this.state.principal },
      { type: 'bar', label: 'Shares Value If Dividends Reinvested', backgroundColor: '#eb34d2', data: this.state.reinvestedDividends }]
    }

    var stackedOptions1 = {
      tooltips: { mode: 'index', intersect: false },
      responsive: true,
      scales: { xAxes: [{ stacked: true, }] }
    };
    
    if (this.state.showSpinner) { this.showApology(); }


    var highLevelStats = [
      {stat:'Share Value',beginning:this.state.principalStart,ending:this.state.principalEnd,endingReinvest:this.state.principalEndReinvested}
      ,{stat:'Share Count',beginning:this.state.shareCountInitial,ending:this.state.shareCountEnding,endingReinvest:this.state.shareCountReinvested}
      ,{stat:'Dividends Collected',beginning:'',ending:this.state.dividendSum,endingReinvest:this.state.dividendReinvestSum}
      ,{stat:'Total Value, Counting Dividends',beginning:'',ending:this.state.principalEndPlusDividendSum,endingReinvest:this.state.principalEndReinvested}
      ,{stat:'CAGR - Compound Annual Growth Rate',beginning:'',ending:this.state.cagr+'%',endingReinvest:this.state.cagrReinvested+'%'}
    ];

    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <table width="500px">
                  <tr>
                    <td>Select Your Business</td>
                    <td><Dropdown
                      value={this.state.ticker}
                      options={this.state.tickerOptions}
                      onChange={(e) => { this.setState({ ticker: e.value });   }}
                      placeholder="Select a Business" />
                    </td>
                  </tr>
                  <tr><td>Start Date</td>
                    <td> <Calendar value={this.state.dateBegin} onChange={(e) => {this.setState({ dateBegin: e.value });}} readOnlyInput="true" required="true" view="month" dateFormat="mm/yy" yearNavigator={true} yearRange="1900:2030" />
                    </td>
                  </tr>
                  <tr><td>End Date</td>
                    <td> <Calendar value={this.state.dateEnd} onChange={(e) => {this.setState({ dateEnd: e.value }); }} readOnlyInput="true" required="true" view="month" dateFormat="mm/yy" yearNavigator={true} yearRange="1900:2030" />
                    </td>
                  </tr>
                  <tr>
                    <td>Initial Investment</td>
                    <td>
                      <InputText keyfilter="int" value={this.state.initialInvestment} onChange={(e) =>{ this.setState({ initialInvestment: e.target.value });}} />
                    </td>
                  </tr>                  
                  <tr>
                    <td>Additional Monthly Investment</td>
                    <td>
                      <InputText keyfilter="int" value={this.state.monthlyInvestment} onChange={(e) =>{ this.setState({ monthlyInvestment: e.target.value });}} />
                    </td>
                  </tr>
                  <tr><td colSpan="2"><Button label="Search"  onClick={(e) => { this.reloadData(); }}/></td></tr>
                </table>
                        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <Growl ref={(el) => this.growl = el} />
                <Messages ref={(el) => this.messages = el}></Messages>


                <h3>Business : {this.state.ticker}</h3>
                <table width="100%" >
                  <tr>
                    <td>Stock and Dividends</td>
                  </tr>
                  <tr>
                    <td>
                    {this.state.showSpinner ? <ProgressSpinner /> : <Chart type="bar" data={stackedData1} options={stackedOptions1} />} 
                    {this.state.showSpinner ? <br /> : " "} 
                    {this.state.showSpinner ? <Message severity="warn" text="Just a Moment."></Message> : " "} 
                    </td>
                  </tr>
                </table>
              </div>

            <DataTable value={highLevelStats} >
              <Column field="stat" header="Stat" />
              <Column field="beginning" header="Beginning" />
              <Column field="ending" header="Ending" />
              <Column field="endingReinvest" header="Ending with Dividends Reinvested" />
            </DataTable>
            </td>
          </tr>
          <tr>
            <td>
            <DataTable value={this.state.dividendYearlyStats} >
              <Column field="year" header="Year" />
              <Column field="divPerShare" header="Dividend Per Share" />
              <Column field="divGrowth" header="Dividend Growth" />
              <Column field="divCollected" header="Dividends Collected" />
              <Column field="divCollectedReinvest" header="Dividends Collected If Reinvested" />
            </DataTable>
            </td>
          </tr>
        </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoBusinessAnalyzer2;
