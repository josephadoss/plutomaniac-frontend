//import React from 'react';


import React, { Component } from 'react';
//import { render } from 'react-dom';
import { Button } from 'primereact/button';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { Growl } from 'primereact/growl';
import { InputText } from 'primereact/inputtext';
import { Chart } from 'primereact/chart';
import {Column} from 'primereact/column';
import {DataTable} from 'primereact/datatable';
import {Messages} from 'primereact/messages';
import {Message} from 'primereact/message';
import { ProgressSpinner } from 'primereact/progressspinner';


//import PlutoStockHistory from './PlutoStockHistory/PlutoStockHistory';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';


class SelectStock extends Component {
  //this.state= { stackedData : {} , stackedOptions:{} };

  constructor(props) {
    super(props);


    this.state = {
      ticker: 'XOM',
      tickers: [
        { value: 'ABBV', label: 'ABBV - AbbVie' }
        , { value: 'ABT', label: 'ABT - Abbott Labs' }
        , { value: 'AEP', label: 'AEP - American Electric Power' }
        , { value: 'AXP', label: 'AXP - American Express' }
        , { value: 'BBL', label: 'BBL - BHP Group' }
        , { value: 'BDX', label: 'BDX - Becton Dickinson' }
        , { value: 'BP', label: 'BP - BP' }
        , { value: 'CAH', label: 'CAH - Cardinal Health' }
        , { value: 'CAT', label: 'CAT - Caterpillar' }
        , { value: 'CHD', label: 'CHD - Church & Dwight' }
        , { value: 'CL', label: 'CL - Colgate Palmolive' }
        , { value: 'CLX', label: 'CLX - Clorox' }
        , { value: 'COP', label: 'COP - ConocoPhillips' }
        , { value: 'CVX', label: 'CVX - Chevron Corporation' }
        , { value: 'DIS', label: 'DIS - The Walt Disney Company' }
        , { value: 'EMR', label: 'EMR - Emerson Electric Co.' }
        , { value: 'GE', label: 'GE - General Electric' }
        , { value: 'GIS', label: 'GIS - General Mills' }
        , { value: 'GSK', label: 'GSK - GlaxoSmithKline' }
        , { value: 'HSY', label: 'HSY - The Hershey Company' }
        , { value: 'JNJ', label: 'JNJ - Johnson & Johnson' }
        , { value: 'K', label: 'K - Kellogg\'s' }
        , { value: 'KO', label: 'KO - Coca Cola' }
        , { value: 'MA', label: 'MA - Mastercard' }
        , { value: 'MCD', label: 'MCD - McDonald\'s' }
        , { value: 'MKC', label: 'MKC - McCormick & Company' }
        , { value: 'MMM', label: 'MMM - 3M' }
        , { value: 'NSRGF', label: 'NSRGF - Nestlé' }
        , { value: 'NSRGY', label: 'NSRGY - Nestlé' }
        , { value: 'O', label: 'O - Realty Income' }
        , { value: 'PEP', label: 'PEP - PepsiCo' }
        , { value: 'PG', label: 'PG - Procter & Gamble' }
        , { value: 'PM', label: 'PM - Philip Morris International' }
        , { value: 'RDS.B', label: 'RDS.B - Royal Dutch Shell' }
        , { value: 'SJM', label: 'SJM - The J.M. Smucker Company' }
        , { value: 'SO', label: 'SO - Southern Company' }
        , { value: 'T', label: 'T - AT&T' }
        , { value: 'UL', label: 'UL - Unilever' }
        , { value: 'UN', label: 'UN - Unilever' }
        , { value: 'V', label: 'V - Visa' }
        , { value: 'WBA', label: 'WBA - Walgreens Boots Alliance' }
        , { value: 'WDFC', label: 'WDFC - WD-40 Company' }
        , { value: 'WTR', label: 'WTR - Aqua America' }
        , { value: 'XOM', label: 'XOM - Exxon' }
        , { value: 'YORW', label: 'YORW - The York Water Company' }
      ]     
    };

  }


  setTicker(t)
  {
    this.setState({ticker:t});
    this.props.updateParentTicker(t);
  }

  render() {


    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

    <Dropdown
                      value={this.state.ticker}
                      options={this.state.tickers}
                      onChange={(e) => { this.setTicker(e.value) }}
                      placeholder="Select a Business" />
                
      </div>
    )
  }
}

export default SelectStock;
