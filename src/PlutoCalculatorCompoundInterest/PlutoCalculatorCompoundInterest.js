import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './PlutoCalculatorCompoundInterest.css';

class PlutoCalculatorCompoundInterest extends Component {
  constructor() {
    super();

    this.state = {
      principal: 100.0,
      rate: 10.0,
      time: 30.0,
      addition : 10.0,
      futurevalue: 3554.37
    };

    this.doClick();
  }

  doClick() {
    var newP = parseFloat(this.state.principal);

    for(var i = 0; i < this.state.time; i++)
    {
      newP += parseFloat(this.state.addition);
      newP *= parseFloat(( this.state.rate/100.0 + 1.0));
    }

    newP = newP.toFixed(2);
    this.setState({futurevalue:newP});
  }

  render() { 
    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <table width="500px">
                  <tr>
                    <td>Principal $</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.principal} onChange={(e) => { this.setState({ principal: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Rate %</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.rate} onChange={(e) => { this.setState({ rate: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Times Compounded ( usually years )</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.time} onChange={(e) => { this.setState({ time: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Additional Annual Contributions <br />( added before the compound calculation )</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.addition} onChange={(e) => { this.setState({ addition: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr><td colSpan="2"><Button label="Compute" onClick={(e) => { this.doClick(); }} /></td></tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <div className="content-section implementation">

                      <h3>Future Value : ${this.state.futurevalue}</h3>

                      </div>

                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoCalculatorCompoundInterest;
