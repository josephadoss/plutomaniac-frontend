import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {InputText} from 'primereact/inputtext';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './PlutoCalculator401kHistory.css';

class PlutoCalculator401kHistory extends Component {
  constructor() {
    super();

    this.state = {
      endingValue:0.0,
      totalContributions:0.0,
      yearlyInterest:7.0,
      contributions:[
        {year:2020,value:0.0,employee:19500.0,catchup:6500.0,total:57000.0,endOfYearValue:0.0},
        {year:2019,value:0.0,employee:19000.0,catchup:6000.0,total:56000.0,endOfYearValue:0.0},
        {year:2018,value:0.0,employee:18500.0,catchup:6000.0,total:55000.0,endOfYearValue:0.0},
        {year:2017,value:0.0,employee:18000.0,catchup:6000.0,total:54000.0,endOfYearValue:0.0},
        {year:2016,value:0.0,employee:18000.0,catchup:6000.0,total:53000.0,endOfYearValue:0.0},
        {year:2015,value:0.0,employee:18000.0,catchup:5500.0,total:53000.0,endOfYearValue:0.0},
        {year:2014,value:0.0,employee:17500.0,catchup:5500.0,total:52000.0,endOfYearValue:0.0},
        {year:2013,value:0.0,employee:17000.0,catchup:5500.0,total:51000.0,endOfYearValue:0.0},
        {year:2012,value:0.0,employee:17000.0,catchup:5500.0,total:50000.0,endOfYearValue:0.0},
        {year:2011,value:0.0,employee:16500.0,catchup:5500.0,total:49000.0,endOfYearValue:0.0},
        {year:2010,value:0.0,employee:16500.0,catchup:5500.0,total:49000.0,endOfYearValue:0.0},
        {year:2009,value:0.0,employee:16500.0,catchup:5500.0,total:49000.0,endOfYearValue:0.0},
        {year:2008,value:0.0,employee:15500.0,catchup:5000.0,total:46000.0,endOfYearValue:0.0},
        {year:2007,value:0.0,employee:15500.0,catchup:5000.0,total:45000.0,endOfYearValue:0.0},
        {year:2006,value:0.0,employee:15000.0,catchup:5000.0,total:44000.0,endOfYearValue:0.0},
        {year:2005,value:0.0,employee:14000.0,catchup:4000.0,total:42000.0,endOfYearValue:0.0},
        {year:2004,value:0.0,employee:13000.0,catchup:3000.0,total:41000.0,endOfYearValue:0.0},
        {year:2003,value:0.0,employee:12000.0,catchup:2000.0,total:40000.0,endOfYearValue:0.0},
        {year:2002,value:0.0,employee:11000.0,catchup:1000.0,total:40000.0,endOfYearValue:0.0},
        {year:2001,value:0.0,employee:10500.0,catchup:0.0,total:35000.0,endOfYearValue:0.0},
        {year:2000,value:0.0,employee:10500.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1999,value:0.0,employee:10000.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1998,value:0.0,employee:10000.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1997,value:0.0,employee:9500.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1996,value:0.0,employee:9500.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1995,value:0.0,employee:9240.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1994,value:0.0,employee:9240.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1993,value:0.0,employee:8994.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1992,value:0.0,employee:8728.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1991,value:0.0,employee:8475.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1990,value:0.0,employee:7979.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1989,value:0.0,employee:7627.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1988,value:0.0,employee:7313.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1987,value:0.0,employee:7000.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1986,value:0.0,employee:7000.0,catchup:0.0,total:30000.0,endOfYearValue:0.0},
        {year:1985,value:0.0,employee:30000.0,catchup:0.0,total:60000.0,endOfYearValue:0.0},
        {year:1984,value:0.0,employee:30000.0,catchup:0.0,total:60000.0,endOfYearValue:0.0},
        {year:1983,value:0.0,employee:30000.0,catchup:0.0,total:60000.0,endOfYearValue:0.0},
        {year:1982,value:0.0,employee:30000.0,catchup:0.0,total:60000.0,endOfYearValue:0.0},
        {year:1981,value:0.0,employee:45575.0,catchup:0.0,total:91150.0,endOfYearValue:0.0},
        {year:1980,value:0.0,employee:45575.0,catchup:0.0,total:91150.0,endOfYearValue:0.0},
        {year:1979,value:0.0,employee:45575.0,catchup:0.0,total:91150.0,endOfYearValue:0.0},
        {year:1978,value:0.0,employee:45575.0,catchup:0.0,total:91150.0,endOfYearValue:0.0}
            ]
    };

    this.editor = this.editor.bind(this);
  }


  editor(props) { 
    return this.inputTextEditor(props, 'startval');
}

inputTextEditor(props, field) { 
  return <InputText type="pnum" value={props.rowData[field]} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />;
}

onEditorValueChange(props, value) { 
  var newCon = this.state.contributions;
  var newVal = value.replace(/\D/g,'');
  for( var i in this.state.contributions)
  { 
    var row = this.state.contributions[i];
    if(row.year===props.rowData.year)
    {
      if(newVal>props.rowData.total)
      {newVal=props.rowData.total;}

      newCon[i].value=newVal;
      this.setState({contributions:newCon});
    }
  }


  var newTotalContributions = 0.0;
  for( var i in this.state.contributions)
  { 
    var row = this.state.contributions[i];
    newTotalContributions+=parseFloat(row.value);
  }
  this.setState({totalContributions:newTotalContributions});

  
  var newEndingValue =parseFloat(0.0);
  for( var i in this.state.contributions)
  { 
    var newI = this.state.contributions.length-i-1;
    var row = this.state.contributions[newI];
    newEndingValue+=parseFloat(row.value);
    newEndingValue*=(parseFloat((this.state.yearlyInterest)/100.0)+1.0);
    newCon[newI].endOfYearValue=newEndingValue.toFixed(2);
    this.setState({contributions:newCon});
  }
  this.setState({endingValue:newEndingValue.toFixed(2)});  
}

  render() {
    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%" >
          <tr>
            <td>Yearly Interest</td>
            <td>&nbsp;</td>
            <td>
              <InputText keyfilter="pnum" value={this.state.yearlyInterest} onChange={(e) => { this.setState({ yearlyInterest: e.target.value }); }} size='5' />%
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>Total Contributions</td>
            <td>&nbsp;</td>
            <td>{this.state.totalContributions}</td>
          </tr>
          <tr>
            <td>Ending Value</td>
            <td>&nbsp;</td>
            <td>{this.state.endingValue}</td>
          </tr>
        </table>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <br />
                <DataTable value={this.state.contributions} editable={true}>
                        <Column field="year" header="Year" style={{height: '3.5em', width:'70px'}}/>
                        <Column field="value" header="My Contributions, Click To Edit, Enter To Submit" editor={this.editor} style={{height: '3.5em', width:'150px'}}/>
                        <Column field="employee" header="Employee Contribution Max" style={{height: '3.5em', width:'110px'}}/>
                        <Column field="catchup" header="Catchup" style={{height: '3.5em', width:'80px'}}/>
                        <Column field="total" header="Total" style={{height: '3.5em', width:'65px'}}/>
                        <Column field="endOfYearValue" header="Balance" style={{height: '3.5em', width:'100px'}}/>
                </DataTable>
              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoCalculator401kHistory;
