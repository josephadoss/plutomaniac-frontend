import React, { Component } from 'react';
import { render } from 'react-dom';
import { Button } from 'primereact/button';
import { Calendar } from 'primereact/calendar';
import { Chart } from 'primereact/chart';
import {Column} from 'primereact/column';
import {ColumnGroup} from 'primereact/columngroup';
import {DataTable} from 'primereact/datatable';
import { Dropdown } from 'primereact/dropdown';
import { Growl } from 'primereact/growl';
import { InputText } from 'primereact/inputtext';
import {Messages} from 'primereact/messages';
import {Message} from 'primereact/message';
import {Row} from 'primereact/row';
import { ProgressSpinner } from 'primereact/progressspinner';

import './node_modules/primereact/resources/themes/nova-light/theme.css';
import './node_modules/primereact/resources/primereact.min.css';
import './node_modules/primeicons/primeicons.css';
import './Pluto10KViewer.css';

class Pluto10KViewer extends Component {
  constructor() {
    super();
    this.state = {
      ticker: 'XOM',
      tickerOptions: [
        { value: 'ABBV', label: 'ABBV - AbbVie' }
        , { value: 'ABT', label: 'ABT - Abbott Labs' }
        , { value: 'AEP', label: 'AEP - American Electric Power' }
        , { value: 'AXP', label: 'AXP - American Express' }
        , { value: 'BDX', label: 'BDX - Becton Dickinson' }
        , { value: 'BP', label: 'BP - BP' }
        , { value: 'CAT', label: 'CAT - Caterpillar' }
        , { value: 'CHD', label: 'CHD - Church & Dwight' }
        , { value: 'CL', label: 'CL - Colgate Palmolive' }
        , { value: 'CLX', label: 'CLX - Clorox' }
        , { value: 'COP', label: 'COP - ConocoPhillips' }
        , { value: 'CVX', label: 'CVX - Chevron Corporation' }
        , { value: 'DIS', label: 'DIS - The Walt Disney Company' }
        , { value: 'EMR', label: 'EMR - Emerson Electric Co.' }
        , { value: 'GE', label: 'GE - General Electric' }
        , { value: 'GIS', label: 'GIS - General Mills' }
        , { value: 'GSK', label: 'GSK - GlaxoSmithKline' }
        , { value: 'HSY', label: 'HSY - The Hershey Company' }
        , { value: 'JNJ', label: 'JNJ - Johnson & Johnson' }
        , { value: 'K', label: 'K - Kellogg\'s' }
        , { value: 'KO', label: 'KO - Coca Cola' }
        , { value: 'MA', label: 'MA - Mastercard' }
        , { value: 'MCD', label: 'MCD - McDonald\'s' }
        , { value: 'MKC', label: 'MKC - McCormick & Company' }
        , { value: 'MMM', label: 'MMM - 3M' }
        , { value: 'NSRGF', label: 'NSRGF - Nestlé' }
        , { value: 'NSRGY', label: 'NSRGY - Nestlé' }
        , { value: 'O', label: 'O - Realty Income' }
        , { value: 'PEP', label: 'PEP - PepsiCo' }
        , { value: 'PG', label: 'PG - Procter & Gamble' }
        , { value: 'PM', label: 'PM - Philip Morris International' }
        , { value: 'RDS.B', label: 'RDS.B - Royal Dutch Shell' }
        , { value: 'SJM', label: 'SJM - The J.M. Smucker Company' }
        , { value: 'SO', label: 'SO - Southern Company' }
        , { value: 'T', label: 'T - AT&T' }
        , { value: 'UN', label: 'UN - Unilever' }
        , { value: 'V', label: 'V - Visa' }
        , { value: 'WBA', label: 'WBA - Walgreens Boots Alliance' }
        , { value: 'WDFC', label: 'WDFC - WD-40 Company' }
        , { value: 'WTR', label: 'WTR - Aqua America' }
        , { value: 'XOM', label: 'XOM - Exxon' }
        , { value: 'YORW', label: 'YORW - The York Water Company' }
      ],
      showSpinner: true,
      edgarStat:[] 
    };

    //this.clearData();

    this.reloadData('XOM'); //alert("constructor");
    this.showApology = this.showApology.bind(this);
  }

  clearData() {
    this.setState({ edgarStat : [] });
  }



  reloadData(newTicker) {
    var ticker = newTicker; //this.state.ticker;
    var params = "?ticker=" + ticker ;
    var url = "./server/edgarData.php" + params;

    if (window.location.href.includes('localhost')) { url = "https://plutomaniac.com/server/edgarData.php" + params; }

    //alert(url);
    var comp = this;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        //document.getElementById("ajax").innerHTML = xhr.responseText;
        //alert(xhr.responseText);
        var json = JSON.parse(xhr.responseText);
        //alert(json);

        //alert("labels:"+json.labels);
        //alert("principal:"+json.principal);
        //alert("dividends:"+json.dividends);

        comp.setState({ edgarStat: json });
        comp.setState({ showSpinner: false });
      }
    };
    xhr.open("GET", url, true);
    xhr.send(null);

    this.setState({ showSpinner: true });
    // alert("selected value : "+this.refs.selectTicker.inputValue);
  }



  setTicker(b){
    this.setState({edgarStat:[]});
    this.setState({ticker:b});
    this.reloadData(b);
    //alert(b);
  }

  showApology() { 
   // this.growl.show({ severity: 'warn', summary: 'Apology', detail: 'Sorry about the long wait.' }); 
  }

  render() {

    if (this.state.showSpinner) { this.showApology(); }

    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <Growl ref={(el) => this.growl = el} />
                <Messages ref={(el) => this.messages = el}></Messages>
                <table width="500px">
                  <tr>
                    <td>Select Your Business</td>
                    <td><Dropdown
                      value={this.state.ticker}
                      options={this.state.tickerOptions}
                      onChange={(e) => {  this.setTicker(e.value);  }}
                      placeholder="Select a Business" />
                    </td>
                  </tr>
                  </table>

              </div>

              {this.state.showSpinner ? <ProgressSpinner /> : ''} 


            <DataTable value={this.state.edgarStat}  
              resizableColumns={true} 
              columnResizeMode="expand"
              selectionMode="single"
              scrollable={true} 
              style={{ width: '100%'}}
              reorderableRows={true} onRowReorder={(e) => this.setState({edgarStat: e.value})}
              >
              <Column rowReorder={true} style={{width: '3em'}} />
              <Column field="field" header="Year Filed ( Not Fiscal Year )" />
              <Column field="2019" header="2019" />
              <Column field="2018" header="2018" />
              <Column field="2017" header="2017" />
              <Column field="2016" header="2016" />
              <Column field="2015" header="2015" />
              <Column field="2014" header="2014" />
              <Column field="2013" header="2013" />
              <Column field="2012" header="2012" />
              <Column field="2011" header="2011" />
              <Column field="2010" header="2010" />
            </DataTable>

            </td>

          </tr>
        </table>
      </div>
    )
  }
}

export default Pluto10KViewer;
