import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './PlutoCalculatorIPRT.css';

class PlutoCalculatorIPRT extends Component {
  constructor() {
    super();

    this.state = {
      principal: 100.0,
      rate: 10.0,
      time: 5.0,
      interest: 50.0
    };

    this.doClick();
  }

  doClick() {
    var P = this.state.principal;
    var r = this.state.rate / 100.0;
    var t = this.state.time;

    var I = P*r*t;

    this.setState({interest:I});
  }

  render() { 
    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <table width="500px">
                  <tr>
                    <td>Principal $</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.principal} onChange={(e) => { this.setState({ principal: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Rate %</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.rate} onChange={(e) => { this.setState({ rate: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Times Compounded ( usually years )</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.time} onChange={(e) => { this.setState({ time: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr><td colSpan="2"><Button label="Compute" onClick={(e) => { this.doClick(); }} /></td></tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <div className="content-section implementation">

                        <h3>Interest You're Going To Pay : ${this.state.interest}</h3>

                      </div>

                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoCalculatorIPRT;
