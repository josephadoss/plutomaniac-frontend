import React, { Component } from 'react';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dropdown } from 'primereact/dropdown';
import { Growl } from 'primereact/growl';
import { Messages } from 'primereact/messages';
import { Panel } from 'primereact/panel';
import { ProgressSpinner } from 'primereact/progressspinner';
import SelectStock from '../SelectStock';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './Pluto10KViewer.css';

class Pluto10KViewer extends Component {
  constructor() {
    super();
    this.state = {
      ticker: 'XOM',
      showSpinner: true,
      edgarStat: [],
      selectedStat: []
    };

    this.reloadData('XOM');
    this.showApology = this.showApology.bind(this);
    this.setTicker = this.setTicker.bind(this);
  }

  clearData() {
    this.setState({ edgarStat: [] });
  }

  displaySelection(data) {
    try {
      if (!data || data.length === 0) {
        return <div style={{ textAlign: 'left' }}>No Selection</div>;
      }
      else {
        if (data instanceof Array)
          return "";
        else
          var newStats = [];
        var firstValue = 0;
        var lastValue = 0;
        var years = 0;
        var growth = 0.0;
        for (var y = 1993; y <= 2020; y++) {
          try {
            var value = data[y].replace(/[^\d.-]/g, '');
            var lastYearsValue = 0.0;

            try { lastYearsValue = data[(y - 1)].replace(/[^\d.-]/g, ''); } catch{ lastYearsValue = 0.0; }

            if (lastYearsValue === '') { lastYearsValue = 0.0; }

            if (value === '' || value === '--') { continue; }

            if (firstValue === 0) { firstValue = value; }

            lastValue = value;

            growth = (value - lastYearsValue) / lastYearsValue * 100.0;
            growth = growth.toFixed(2);
            growth += "%";

            newStats.unshift({ "year": y, "value": value, "growth": growth });
            years++;
          } catch{ }
        }

        var cagr = Math.pow((lastValue / firstValue), (1.0 / years)) - 1.0;
        cagr *= 100.0;
        cagr = cagr.toFixed(2);
        cagr += "%";
        newStats.unshift({ "year": data["field"], "value": "CAGR", "growth": cagr });

        growth = (lastValue - firstValue) / firstValue * 100.0;
        growth = growth.toFixed(2);
        growth += "%";
        newStats.unshift({ "year": data["field"], "value": "Total Growth", "growth": growth });

        this.setState({ selectedStat: newStats });
        return <div style={{ textAlign: 'left' }}>Selected Row: {'Selected : ' + data.field}</div>
      }
    } catch{ }
  }

  reloadData(newTicker) {
    var ticker = newTicker;
    var params = "?ticker=" + ticker;
    var url = "./server/edgarData.php" + params;

    if (window.location.href.includes('localhost')) { url = "https://plutomaniac.com/server/edgarData.php" + params; }

    var comp = this;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        //alert(xhr.responseText);
        var json = JSON.parse(xhr.responseText);

        comp.setState({ edgarStat: json });
        comp.setState({ showSpinner: false });
      }
    };
    xhr.open("GET", url, true);
    xhr.send(null);

    this.setState({ showSpinner: true });
  }

  setTicker(b) {
    this.setState({ edgarStat: [] });
    this.setState({ ticker: b });
    this.reloadData(b);
  }

  showApology() {
    // this.growl.show({ severity: 'warn', summary: 'Apology', detail: 'Sorry about the long wait.' }); 
  }

  render() {

    if (this.state.showSpinner) { this.showApology(); }

    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <Panel header="Disclaimer Disclaimer!!!">
                This data is pulled from the SEC's EDGAR database... or perhaps 'filebase' would be the more accurate term. There is emense variation amongst companies as to what they report and how they label it. There's even variation across years by the same company. For example, profits might be called Profits, NetIncome, NetIncomeLoss, GrossProfit, NetProfit, NetSales, RetainedEarnings, or anything other than the actual word 'Profit' so as to make investors' lives more confusing than necessary. After a few years, they'll use another word so it looks as if they stopped reporting that value.
                <br /><br /> Sometimes they report a number like 23,233 with a note at the top saying these numbers are in millions. So 23,233,000,000. But I can't multiply everything on the page by 1,000,000 because they'll report values that are actualy dollar values, expecting that to not confuse anyone...
                <br /><br /> One day I'll work out which are Assets, Liabilities, Equity, Revenue, Expenses, and Profit for each business and put them at the top. Until then you can dig through the following or follow the R2.htm link to that year's Consolidated Income statement. Unless they report that in R4.htm.
                <br /><br /> In short : I still have a lot of work to do on this page.
                <br /><br /> Click on a row and scroll to the table at the bottom to view the year over year growth for that value.
              </Panel>
            </td>
          </tr>
          <tr><td><br /><br /></td></tr>
          <tr>
            <td>
              <div className="content-section implementation">
                <Growl ref={(el) => this.growl = el} />
                <Messages ref={(el) => this.messages = el}></Messages>
                <table width="500px">
                  <tr>
                    <td>Select Your Business</td>
                    <td>
                      <SelectStock updateParentTicker={this.setTicker} />
                    </td>
                  </tr>
                </table>

              </div>

              {this.state.showSpinner ? <ProgressSpinner /> : ''}


              <DataTable value={this.state.edgarStat}
                resizableColumns={true}
                columnResizeMode="expand"
                selectionMode="single"
                scrollable={true}
                style={{ width: '1000px' }}
                reorderableRows={true} onRowReorder={(e) => this.setState({ edgarStat: e.value })}
                onSelectionChange={e => this.displaySelection(e.value)}//this.setState({selectedRow : e.value})}
                //footer={this.displaySelection(this.state.selectedRow)}
                scrollHeight="400px"
              >
                <Column rowReorder={true} style={{ width: '3em' }} />
                <Column style={{ width: '350px' }} field="field" header="Year Filed ( Not Fiscal Year ) &#8594;" />
                <Column field="2020" header="2020" />
                <Column field="2019" header="2019" />
                <Column field="2018" header="2018" />
                <Column field="2017" header="2017" />
                <Column field="2016" header="2016" />
                <Column field="2015" header="2015" />
                <Column field="2014" header="2014" />
                <Column field="2013" header="2013" />
                <Column field="2012" header="2012" />
                <Column field="2011" header="2011" />
                <Column field="2010" header="2010" />
              </DataTable>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>
              <DataTable value={this.state.selectedStat}
                resizableColumns={true}
                columnResizeMode="expand"
                scrollable={true}
                style={{ width: '100%' }}
              >
                <Column field="year" header="Year Filed ( Not Fiscal Year ) &#8595;" />
                <Column field="value" header="Value" />
                <Column field="growth" header="Growth" />
              </DataTable>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default Pluto10KViewer;
