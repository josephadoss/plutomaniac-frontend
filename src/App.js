import React, { Component } from 'react';
import classNames from 'classnames';
import { AppTopbar } from './AppTopbar';
import { AppFooter } from './AppFooter';
import { AppMenu } from './AppMenu';
import { AppProfile } from './AppProfile';
import { Route } from 'react-router-dom';
import { Dashboard } from './components/Dashboard';
import { FormsDemo } from './components/FormsDemo';
import { SampleDemo } from './components/SampleDemo';
import { DataDemo } from './components/DataDemo';
import { PanelsDemo } from './components/PanelsDemo';
import { OverlaysDemo } from './components/OverlaysDemo';
import { MenusDemo } from './components/MenusDemo';
import { MessagesDemo } from './components/MessagesDemo';
import { ChartsDemo } from './components/ChartsDemo';
import { MiscDemo } from './components/MiscDemo';
import { EmptyPage } from './components/EmptyPage';
import { Documentation } from "./components/Documentation";
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import './layout/layout.scss';
import './App.scss';
import './App.css';
import AdSense from 'react-adsense';
import ReactGA from 'react-ga';
import PlutoBusinessAnalyzer2 from './PlutoBusinessAnalyzer2/PlutoBusinessAnalyzer2';
import PlutoStockMarketAndPolitics from './PlutoStockMarketAndPolitics/PlutoStockMarketAndPolitics';
import Pluto10KViewer from './Pluto10KViewer/Pluto10KViewer';
import PlutoChoroplethTest from './PlutoChoroplethTest/PlutoChoroplethTest';
import PlutoCalculatorCAGR from './PlutoCalculatorCAGR/PlutoCalculatorCAGR';
import PlutoCalculatorIPRT from './PlutoCalculatorIPRT/PlutoCalculatorIPRT';
import PlutoCalculatorPennyADay from './PlutoCalculatorPennyADay/PlutoCalculatorPennyADay';
import PlutoCalculator401kHistory from './PlutoCalculator401kHistory/PlutoCalculator401kHistory';
import PlutoCalculatorIRAHistory from './PlutoCalculatorIRAHistory/PlutoCalculatorIRAHistory';
import PlutoCalculatorCompoundInterest from './PlutoCalculatorCompoundInterest/PlutoCalculatorCompoundInterest';
import PlutoCalculatorW9TaxCalculator2019 from './PlutoCalculatorW9TaxCalculator2019/PlutoCalculatorW9TaxCalculator2019';
import PlutoCalculatorW9TaxCalculator2020 from './PlutoCalculatorW9TaxCalculator2020/PlutoCalculatorW9TaxCalculator2020';



class App extends Component {

    constructor() {
        super();
        ReactGA.initialize('UA-131303158-1');
        ReactGA.pageview(window.location.pathname + window.location.search);
        this.state = {
            layoutMode: 'static',
            layoutColorMode: 'dark',
            staticMenuInactive: false,
            overlayMenuActive: false,
            mobileMenuActive: false
        };

        this.onWrapperClick = this.onWrapperClick.bind(this);
        this.onToggleMenu = this.onToggleMenu.bind(this);
        this.onSidebarClick = this.onSidebarClick.bind(this);
        this.onMenuItemClick = this.onMenuItemClick.bind(this);
        this.createMenu();


    }


    onWrapperClick(event) { //alert("onwrapperclick");
        if (!this.menuClick) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            });
        }

        this.menuClick = false;
    }

    onToggleMenu(event) {
        this.menuClick = true;

        if (this.isDesktop()) {
            if (this.state.layoutMode === 'overlay') {
                this.setState({
                    overlayMenuActive: !this.state.overlayMenuActive
                });
            }
            else if (this.state.layoutMode === 'static') {
                this.setState({
                    staticMenuInactive: !this.state.staticMenuInactive
                });
            }
        }
        else {
            const mobileMenuActive = this.state.mobileMenuActive;
            this.setState({
                mobileMenuActive: !mobileMenuActive
            });
        }

        event.preventDefault();
    }

    onSidebarClick(event) {// alert("onsidebarclick");
        this.menuClick = true;
    }

    onMenuItemClick(event) {
        if (!event.item.items) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            })
        }
    }

    createMenu() {
        this.menu = [
            { label: 'Dashboard', icon: 'pi pi-fw pi-home', command: () => { window.location = '#/' } },
            {
                label: 'Menu Modes', icon: 'pi pi-fw pi-cog',
                items: [
                    { label: 'Static Menu', icon: 'pi pi-fw pi-bars', command: () => this.setState({ layoutMode: 'static' }) },
                    { label: 'Overlay Menu', icon: 'pi pi-fw pi-bars', command: () => this.setState({ layoutMode: 'overlay' }) }
                ]
            },
            {
                label: 'Menu Colors', icon: 'pi pi-fw pi-align-left',
                items: [
                    { label: 'Dark', icon: 'pi pi-fw pi-bars', command: () => this.setState({ layoutColorMode: 'dark' }) },
                    { label: 'Light', icon: 'pi pi-fw pi-bars', command: () => this.setState({ layoutColorMode: 'light' }) }
                ]
            },
            {
                label: 'Components', icon: 'pi pi-fw pi-globe', badge: '9',
                items: [
                    { label: 'Sample Page', icon: 'pi pi-fw pi-th-large', to: '/sample' },
                    { label: 'Forms', icon: 'pi pi-fw pi-file', to: '/forms' },
                    { label: 'Data', icon: 'pi pi-fw pi-table', to: '/data' },
                    { label: 'Panels', icon: 'pi pi-fw pi-list', to: '/panels' },
                    { label: 'Overlays', icon: 'pi pi-fw pi-clone', to: '/overlays' },
                    { label: 'Menus', icon: 'pi pi-fw pi-plus', to: '/menus' },
                    { label: 'Messages', icon: 'pi pi-fw pi-spinner', to: '/messages' },
                    { label: 'Charts', icon: 'pi pi-fw pi-chart-bar', to: '/charts' },
                    { label: 'Misc', icon: 'pi pi-fw pi-upload', to: '/misc' }
                ]
            },
            {
                label: 'Template Pages', icon: 'pi pi-fw pi-file',
                items: [
                    { label: 'Empty Page', icon: 'pi pi-fw pi-circle-off', to: '/empty' }
                ]
            },
            {
                label: 'Menu Hierarchy', icon: 'pi pi-fw pi-search',
                items: [
                    {
                        label: 'Submenu 1', icon: 'pi pi-fw pi-bookmark',
                        items: [
                            {
                                label: 'Submenu 1.1', icon: 'pi pi-fw pi-bookmark',
                                items: [
                                    { label: 'Submenu 1.1.1', icon: 'pi pi-fw pi-bookmark' },
                                    { label: 'Submenu 1.1.2', icon: 'pi pi-fw pi-bookmark' },
                                    { label: 'Submenu 1.1.3', icon: 'pi pi-fw pi-bookmark' },
                                ]
                            },
                            {
                                label: 'Submenu 1.2', icon: 'pi pi-fw pi-bookmark',
                                items: [
                                    { label: 'Submenu 1.2.1', icon: 'pi pi-fw pi-bookmark' },
                                    { label: 'Submenu 1.2.2', icon: 'pi pi-fw pi-bookmark' }
                                ]
                            },
                        ]
                    },
                    {
                        label: 'Submenu 2', icon: 'pi pi-fw pi-bookmark',
                        items: [
                            {
                                label: 'Submenu 2.1', icon: 'pi pi-fw pi-bookmark',
                                items: [
                                    { label: 'Submenu 2.1.1', icon: 'pi pi-fw pi-bookmark' },
                                    { label: 'Submenu 2.1.2', icon: 'pi pi-fw pi-bookmark' },
                                    { label: 'Submenu 2.1.3', icon: 'pi pi-fw pi-bookmark' },
                                ]
                            },
                            {
                                label: 'Submenu 2.2', icon: 'pi pi-fw pi-bookmark',
                                items: [
                                    { label: 'Submenu 2.2.1', icon: 'pi pi-fw pi-bookmark' },
                                    { label: 'Submenu 2.2.2', icon: 'pi pi-fw pi-bookmark' }
                                ]
                            }
                        ]
                    }
                ]
            },
            { label: 'Documentation', icon: 'pi pi-fw pi-question', command: () => { window.location = "#/documentation" } },
            { label: 'View Source', icon: 'pi pi-fw pi-search', command: () => { window.location = "https://github.com/primefaces/sigma" } }
        ];



        this.menu = [
            {
                label: 'Business', icon: 'pi pi-fw pi-money-bill',
                items: [
                    { label: 'Stock History', icon: 'pi pi-fw pi-chart-bar', to: '/stockhistory' },
                    { label: 'Business Fundamentals', icon: 'pi pi-fw pi-briefcase', to: '/edgar' }
                ]
            },
            {
                label: 'Calculators', icon: 'pi pi-fw pi-circle-on',
                items: [
                    { label: 'CAGR', icon: 'pi pi-fw pi-circle-on', to: '/calculatorcagr' },
                    { label: 'Interest Calculator', icon: 'pi pi-fw pi-circle-on', to: '/calculatoriprt' },
                    { label: 'Penny A Day', icon: 'pi pi-fw pi-circle-on', to: '/calculatorpennyaday' },
                    { label: '401k History', icon: 'pi pi-fw pi-circle-on', to: '/calculator401khistory' },
                    { label: 'IRA History', icon: 'pi pi-fw pi-circle-on', to: '/calculatorirahistory' },
                    { label: 'Compound Interest', icon: 'pi pi-fw pi-circle-on', to: '/calculatorcompoundinterest' },
                    { label: 'W9 Taxes 2019', icon: 'pi pi-fw pi-circle-on', to: '/calculatorw9taxes2019' },
                    { label: 'W9 Taxes 2020', icon: 'pi pi-fw pi-circle-on', to: '/calculatorw9taxes2020' }
                ]
            },
            {
                label: 'Politics', icon: 'pi pi-fw pi-money-bill',
                items: [
                    { label: 'Stock Market and Politics', icon: 'pi pi-fw pi-chart-bar', to: '/stockmarketandpolitics' }
                ]
            },
            { label: 'Choropleth Test', icon: 'pi pi-fw pi-map-marker', to: '/choroplethtest' },
            { label: 'Blog', icon: 'pi pi-fw pi-bookmark', command: () => { window.location = "https://blog.plutomaniac.com" } }
        ];
    }

    addClass(element, className) {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    }

    removeClass(element, className) {
        if (element.classList)
            element.classList.remove(className);
        else
            element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    isDesktop() {
        return window.innerWidth > 1024;
    }

    componentDidUpdate() {
        if (this.state.mobileMenuActive)
            this.addClass(document.body, 'body-overflow-hidden');
        else
            this.removeClass(document.body, 'body-overflow-hidden');
    }

    render() {
        const logo = this.state.layoutColorMode === 'dark' ? './logo.png' : './logo.png';

        const wrapperClass = classNames('layout-wrapper', {
            'layout-overlay': this.state.layoutMode === 'overlay',
            'layout-static': this.state.layoutMode === 'static',
            'layout-static-sidebar-inactive': this.state.staticMenuInactive && this.state.layoutMode === 'static',
            'layout-overlay-sidebar-active': this.state.overlayMenuActive && this.state.layoutMode === 'overlay',
            'layout-mobile-sidebar-active': this.state.mobileMenuActive
        });

        const sidebarClassName = classNames("layout-sidebar", {
            'layout-sidebar-dark': this.state.layoutColorMode === 'dark',
            'layout-sidebar-light': this.state.layoutColorMode === 'light'
        });

        return (
            <div className={wrapperClass} >
                <AppTopbar onToggleMenu={this.onToggleMenu} />

                <div ref={(el) => this.sidebar = el} className={sidebarClassName} onClick={this.onSidebarClick}>
                    <div className="layout-logo">
                        <img height='100px' alt="Logo" src={logo} />
                    </div>
                    <AppProfile />
                    <AppMenu model={this.menu} onMenuItemClick={this.onMenuItemClick} />
                </div>

                <div className="layout-main">

                    <table width="100%">
                        <tr>
                            <td>
                                <Route path="/" exact component={PlutoBusinessAnalyzer2} />
                                <Route path="/dash" component={Dashboard} />
                                <Route path="/forms" component={FormsDemo} />
                                <Route path="/sample" component={SampleDemo} />
                                <Route path="/data" component={DataDemo} />
                                <Route path="/panels" component={PanelsDemo} />
                                <Route path="/overlays" component={OverlaysDemo} />
                                <Route path="/menus" component={MenusDemo} />
                                <Route path="/messages" component={MessagesDemo} />
                                <Route path="/charts" component={ChartsDemo} />
                                <Route path="/misc" component={MiscDemo} />
                                <Route path="/empty" component={EmptyPage} />
                                <Route path="/documentation" component={Documentation} />
                                <Route path="/stockhistory" component={PlutoBusinessAnalyzer2} />
                                <Route path="/edgar" component={Pluto10KViewer} />
                                <Route path="/choroplethtest" component={PlutoChoroplethTest} />

                                <Route path="/calculatorcagr" component={PlutoCalculatorCAGR} />
                                <Route path="/calculatoriprt" component={PlutoCalculatorIPRT} />
                                <Route path="/calculatorpennyaday" component={PlutoCalculatorPennyADay} />
                                <Route path="/calculator401khistory" component={PlutoCalculator401kHistory} />
                                <Route path="/calculatorirahistory" component={PlutoCalculatorIRAHistory} />
                                <Route path="/calculatorcompoundinterest" component={PlutoCalculatorCompoundInterest} />

                                <Route path="/calculatorw9taxes2019" component={PlutoCalculatorW9TaxCalculator2019} />
                                <Route path="/calculatorw9taxes2020" component={PlutoCalculatorW9TaxCalculator2020} />

                                
                                <Route path="/stockmarketandpolitics" component={PlutoStockMarketAndPolitics} />
                            </td>

                            <td width="300px">
                                <table>
                                    <tr>
                                        <td>
                                            <AdSense.Google
                                                client='ca-pub-8804678334189979'
                                                slot='4134460638'
                                                style={{ width: 300, height: 800, float: 'right' }}
                                                format='' />
                                        </td>
                                    </tr>
                                    <tr><td><br /><br /><br /><br /><br /><div id="352833732"></div></td></tr>
                                    <tr><td><br /><br /><br /><br /><br />Your Ad Here!</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr><td>Help out the project!</td></tr>
                        <tr><td>Donate Bitcoin </td></tr>
                        <tr><td>1QB1CtxVenaxPq5WQJu4NzvWR8R1gm1Yb4</td></tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr><td>Contact : maniac.at.plutomaniac.dot.com</td></tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr><td>Coming Soon :</td></tr>
                        <tr><td>More Businesses</td></tr>
                        <tr><td>More History</td></tr>
                        <tr><td>Spinoffs</td></tr>
                        <tr><td>More EDGAR Data</td></tr>
                        <tr><td>Choropleths</td></tr>
                        <tr><td>Useful Calculators</td></tr>
                        <tr><td>Blog Articles</td></tr>
                    </table>


                    <AppFooter />
                </div>
                <div className="layout-mask"></div>
            </div>
        );
    }
}
export default App;