import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './PlutoCalculatorCAGR.css';

class PlutoCalculatorCAGR extends Component {
  constructor() {
    super();

    this.state = {
      balanceBeginning: 0.0,
      balanceEnding: 0.0,
      numberOfYears: 0.0,
      cagr: 0.0
    };
  }
  doClick() {
    var a = ( this.state.balanceEnding / this.state.balanceBeginning ); 
    var b = (1.0 / this.state.numberOfYears); 

    var cagr = Math.pow(a, b) - 1.0; 
    cagr *= 100.0;
    cagr = parseFloat(cagr).toFixed(2);
    cagr = cagr + "%";
    this.setState({cagr:cagr});
  }

  render() { 
    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <table width="500px">
                  <tr>
                    <td>Beginning Balance</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.balanceBeginning} onChange={(e) => { this.setState({ balanceBeginning: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Ending Balance</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.balanceEnding} onChange={(e) => { this.setState({ balanceEnding: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Number Of Years</td>
                    <td>
                      <InputText keyfilter="pnum" value={this.state.numberOfYears} onChange={(e) => { this.setState({ numberOfYears: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr><td colSpan="2"><Button label="Compute" onClick={(e) => { this.doClick(); }} /></td></tr>
                </table>
                <table width="100%">
                  <tr>
                    <td>
                      <div className="content-section implementation">

                        <h3>CAGR : {this.state.cagr}</h3>

                      </div>

                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoCalculatorCAGR;
