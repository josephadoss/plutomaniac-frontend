import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './PlutoCalculatorW9TaxCalculator2020.css';

class PlutoCalculatorW9TaxCalculator2020 extends Component {

  constructor() {
    super();

    this.state = {
      adjustedGrossIncome: 0.0,
      adjustedGrossIncomePercent: 0.9235,
      federalTaxOwed: 0.0,
      federalTaxPercentage: { name: 'Progressive', code: 'Progressive' },
      grossIncome: 50000.0,
      louisianaTaxOwed: 0.0,
      louisianaTaxPercentage: { name: 'Progressive', code: 'Progressive' },
      medicareOwed: 0.0,
      medicarePercent: 0.029,
      percentKept:0.0,
      remaining:0.0,
      socialSecurityOwed: 0.0,
      socialSecurityPercent: 0.124
    };

    this.onFederalTaxChange = this.onFederalTaxChange.bind(this);

    this.onLouisianaTaxChange = this.onLouisianaTaxChange.bind(this);
    this.doClick();
  }



  doClick() {

    var sso = parseFloat(this.state.grossIncome) * parseFloat(this.state.socialSecurityPercent);
    sso = sso.toFixed(2);
    this.setState({ socialSecurityOwed: sso });

    var mo = parseFloat(this.state.grossIncome) * parseFloat(this.state.medicarePercent);
    mo = mo.toFixed(2);
    this.setState({ medicareOwed: mo });

    var agi = parseFloat(this.state.grossIncome) * parseFloat(this.state.adjustedGrossIncomePercent);
    agi = agi.toFixed(2);
    this.setState({ adjustedGrossIncome: agi });

  
    var fto = 0.0;
    switch (this.state.federalTaxPercentage.code) {
      case "10%":
        fto = parseFloat(.10) * agi;
        break;
      case "12%":
        fto = parseFloat(.12) * agi;
        break;
      case "22%":
        fto = parseFloat(.22) * agi;
        break;
      case "24%":
        fto = parseFloat(.24) * agi;
        break;
      case "32%":
        fto = parseFloat(.32) * agi;
        break;
      case "35%":
        fto = parseFloat(.35) * agi;
        break;
      case "37%":
        fto = parseFloat(.37) * agi;
        break;
      default: // Progressive, so actually figure it out
        var v10 = 9875.0;
        var v12 = 40125.0;
        var v22 = 85525.0;
        var v24 = 163300.0;
        var v32 = 207350.0;
        var v35 = 518400.0;
        var v37 = 99999999.0;

        var a10 = (agi>v10) ?v10:(agi); 
        a10 *= parseFloat(.10);
        var a12 = (agi>v12) ? (v12) :(agi);
        a12 -= parseFloat(v10);
        a12 *= parseFloat(.12);
        var a22 = (agi>v22) ? (v22) :(agi);
        a22 -=parseFloat(v12+v10);
        a22 *= parseFloat(.22);
        var a24 = (agi>v24) ? (v24) :(agi);
        a24 -=parseFloat(v22+v12+v10);
        a24 *= parseFloat(.24);
        var a32 = (agi>v32) ? (v32) :(agi);
        a32 -=parseFloat(v24+v22+v12+v10);
        a32 *= parseFloat(.32);
        var a35 = (agi>v35) ? (v35) :(agi);
        a35 -=parseFloat(v32+v24+v22+v12+v10);
        a35 *= parseFloat(.35);
        var a37 = (agi>v37) ? (v37) :(agi);
        a37 -=parseFloat(v35+v32+v24+v22+v12+v10);
        a37 *= parseFloat(.35);
        
        fto = parseFloat(a10);;
        fto += parseFloat((a12>0.0)?a12:0.0);
        fto += parseFloat((a22>0.0)?a22:0.0);
        fto += parseFloat((a24>0.0)?a24:0.0);
        fto += parseFloat((a32>0.0)?a32:0.0);
        fto += parseFloat((a35>0.0)?a35:0.0);
        fto += parseFloat((a37>0.0)?a37:0.0);
        break;
    }
    fto = fto.toFixed(2);
    this.setState({ federalTaxOwed: fto });
  
  
    var lto = 0.0;
    switch (this.state.louisianaTaxPercentage.code) {
      case '2%':
        lto = parseFloat(.02) * agi;
        break;
      case '4%':
        lto = parseFloat(.04) * agi;
        break;
      case '6%':
        lto = parseFloat(.06) * agi; 
        break;
      default: // Progressive, so actually figure it out
        var v2 = 12500.0;
        var v4 = 50000.0;
        var v6 = 99999999.0;
      
        var a2 = (agi>v2) ?v2:(agi); 
        a2 *= parseFloat(.02);
        var a4= (agi>v4) ? (v4) :(agi);
        a4 -= parseFloat(v2);
        a4 *= parseFloat(.04);
        var a6= (agi>v6) ? (v6) :(agi);
        a6 -=parseFloat(v4);
        a6 *= parseFloat(.06);
        
        lto = parseFloat(a2);
        lto += parseFloat((a4>0.0)?a4:0.0);
        lto += parseFloat((a6>0.0)?a6:0.0);
        break;
    }
    lto = lto.toFixed(2);
    this.setState({ louisianaTaxOwed: lto });
  
    var r = agi - fto - lto;
    r = r.toFixed(2);
    this.setState({remaining:r});

    var pk = parseFloat(r / this.state.grossIncome *100.0).toFixed(2);
    this.setState({percentKept:pk});
  }


  onFederalTaxChange(e) {
    this.setState({ federalTaxPercentage: e.value });
  }

  onLouisianaTaxChange(e) {
    this.setState({ louisianaTaxPercentage: e.value });
  }


  render() { 

    var ssp = (parseFloat(this.state.socialSecurityPercent) * 100.0).toFixed(2);
    ssp += "%";

    var mp = (parseFloat(this.state.medicarePercent) * 100.0).toFixed(2);
    mp += "%";

    var agp = (parseFloat(this.state.adjustedGrossIncomePercent) * 100.0).toFixed(2);
    agp += "%";

    const federalTaxRates = [
      { name: 'Progressive', code: 'Progressive' },
      { name: '10%', code: '10%' },
      { name: '12%', code: '12%' },
      { name: '22%', code: '22%' },
      { name: '24%', code: '24%' },
      { name: '32%', code: '32%' },
      { name: '35%', code: '35%' },
      { name: '37%', code: '37%' }
    ];

    const louisianaTaxRates = [
      { name: 'Progressive', code: 'Progressive' },
      { name: '2%', code: '2%' },
      { name: '4%', code: '4%' },
      { name: '6%', code: '6%' }
    ];

    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <p>This is a simple calculator I wrote to help figure out quarterly taxes for independent contractor income. This tool is no substitute for a tax professional. If your Gross Income is over $132,900, then the social security amount is wrong.</p>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                <table width="600px">
                  <tr>
                    <td>Gross Income </td>
                    <td></td>
                    <td>
                     $<InputText size='7' keyfilter="pnum" value={this.state.grossIncome} onChange={(e) => { this.setState({ grossIncome: e.target.value }); }} />
                    </td>
                  </tr>
                  <tr>
                    <td>Social Security</td>
                    <td>{ssp}</td>
                    <td>
                      ${this.state.socialSecurityOwed}
                    </td>
                  </tr>
                  <tr>
                    <td>Medicare</td>
                    <td>{mp}</td>
                    <td>
                      ${this.state.medicareOwed}
                    </td>
                  </tr>
                  <tr><td><br /></td></tr>
                  <tr>
                    <td>Adjusted Gross Income</td>
                    <td>{agp}</td>
                    <td>
                      ${this.state.adjustedGrossIncome}
                    </td>
                  </tr>
                  <tr><td><br /></td></tr>
                  <tr>
                    <td>Federal Tax</td>
                    <td><Dropdown value={this.state.federalTaxPercentage} options={federalTaxRates} onChange={this.onFederalTaxChange} placeholder="Select a Federal Tax Rate" optionLabel="name" /></td>
                    <td>
                      ${this.state.federalTaxOwed}
                    </td>
                  </tr>
                  <tr>
                    <td>Louisiana State Tax</td>
                    <td><Dropdown value={this.state.louisianaTaxPercentage} options={louisianaTaxRates} onChange={this.onLouisianaTaxChange} placeholder="Select a Louisiana Tax Rate" optionLabel="name" /></td>
                    <td>
                      ${this.state.louisianaTaxOwed}
                    </td>
                  </tr>
                  <tr><td colSpan="2"><Button label="Compute" onClick={(e) => { this.doClick(); }} /></td></tr>
                </table>
                <table width="100%">
                  <tr>
                    <td colSpan='3'>
                      <div className="content-section implementation">

                        <h3>After Tax Income : ${this.state.remaining}</h3>
                        <p>Hello Solo Roth 401k!</p>

                      </div>

                    </td>
                  </tr>
                  <tr><td><br/><br/><br/><br/></td></tr>
                  <tr><td colSpan='3'>Percent Kept : {this.state.percentKept}%</td></tr>
                  <tr><td colSpan='3'>{this.state.percentKept<75.0&&this.state.percentKept>0.0?<p>...grumble...grumble...grouchy Republican</p>:<p></p>}</td></tr>
                  <tr><td><br /><br /><br /><br /></td></tr>
                  <tr><td colSpan='4'> <a href='https://www.eftps.gov/eftps/'>https://www.eftps.gov/eftps/</a></td></tr>
                  <tr><td colSpan='4'><a href='http://www.revenue.louisiana.gov/EServices/LouisianaFileOnline'>http://www.revenue.louisiana.gov/EServices/LouisianaFileOnline</a></td></tr>

                </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoCalculatorW9TaxCalculator2020;
