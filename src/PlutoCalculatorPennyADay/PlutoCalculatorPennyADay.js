import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {InputText} from 'primereact/inputtext';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './PlutoCalculatorPennyADay.css';

class PlutoCalculatorPennyADay extends Component {
  constructor() {
    super();

    this.state = {
      second : 0.01,
      minute : 0.01,
      hour : 0.01,
      day : 0.01,
      week : 0.01,
      month : 0.01,
      quarter : 0.01,
      year : 0.01,
      decade : 0.01,
    };

    this.editor = this.editor.bind(this);
  }


  editor(props) { 
    return this.inputTextEditor(props, 'startval');
}

inputTextEditor(props, field) { 
  return <InputText type="pnum" value={props.rowData[field]} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />;
}

onEditorValueChange(props, value) { 
  switch(props.rowData.field)
  {
    case "Second": this.setState({second:value}); break;
    case "Minute": this.setState({minute:value}); break;
    case "Hour": this.setState({hour:value}); break;
    case "Day": this.setState({day:value}); break;
    case "Week": this.setState({week:value}); break;
    case "Month": this.setState({month:value}); break;
    case "Quarter": this.setState({quarter:value}); break;
    case "Year": this.setState({year:value}); break;
    case "Decade": this.setState({decade:value}); break;
    default : break;
  }
}

  render() { 

  var newSecond = Number.parseFloat(this.state.second);
  var newMinute = Number.parseFloat(this.state.minute );
  var newHour = Number.parseFloat(this.state.hour );
  var newDay = Number.parseFloat(this.state.day );
  var newWeek = Number.parseFloat(this.state.week );
  var newMonth = Number.parseFloat(this.state.month );
  var newQuarter = Number.parseFloat(this.state.quarter );
  var newYear = Number.parseFloat(this.state.year );
  var newDecade = Number.parseFloat(this.state.decade );


  let rows = [
    {field:'Second'
      ,startval:(newSecond).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:(newSecond).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,minute:(newSecond*60).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,hour:(newSecond*60*60).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,day:(newSecond*60*60*24).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,week:(newSecond*60*60*24*7).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,month:(newSecond*60*60*24*365/12).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,quarter:(newSecond*60*60*24*365/4).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newSecond*60*60*24*365).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newSecond*60*60*24*365*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Minute'
      ,startval:(newMinute).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:(newMinute).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,hour:(newMinute*60).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,day:(newMinute*60*24).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,week:(newMinute*60*24*7).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,month:(newMinute*60*24*365/12).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,quarter:(newMinute*60*24*365/4).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newMinute*60*24*365).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newMinute*60*24*365*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Hour'
      ,startval:(newHour).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:(newHour).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,day:(newHour*24).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,week:(newHour*24*7).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,month:(newHour*24*365/12).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,quarter:(newHour*24*365/4).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newHour*24*365).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newHour*24*365*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Day'
      ,startval:(newDay).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:''
      ,day:(newDay).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,week:(newDay*7).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,month:(newDay*365/12).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,quarter:(newDay*365/4).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newDay*365).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newDay*365*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Week'
      ,startval:(newWeek).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:''
      ,day:''
      ,week:(newWeek).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,month:(newWeek/7*365/12).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,quarter:(newWeek/7*365/4).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newWeek/7*365).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newWeek/7*365*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Month'
      ,startval:(newMonth).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:''
      ,day:''
      ,week:''
      ,month:(newMonth).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,quarter:(newMonth*3).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newMonth*12).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newMonth*12*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Quarter'
      ,startval:(newQuarter).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:''
      ,day:''
      ,week:''
      ,month:''
      ,quarter:(newQuarter).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,year:(newQuarter*4).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newQuarter*4*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Year'
      ,startval:(newYear).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:''
      ,day:''
      ,week:''
      ,month:''
      ,quarter:''
      ,year:(newYear).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,decade:(newYear*10).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
    {field:'Decade'
      ,startval:(newDecade).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      ,second:''
      ,minute:''
      ,hour:''
      ,day:''
      ,week:''
      ,month:''
      ,quarter:''
      ,year:''
      ,decade:(newDecade).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
  ];


    return (
      <div>
        <div className="content-section introduction">
          <div className="feature-intro">
          </div>
        </div>

        <table width="100%">
          <tr>
            <td>
              <div className="content-section implementation">
                I know the typing is funny, but it's still a pretty cool tool as it is.
                <br />
              
                <DataTable value={rows} editable={true}>
                        <Column field="field" header="Field" style={{height: '3.5em', width:'100px'}}/>
                        <Column field="startval" header="Click These Cells To Edit" editor={this.editor} style={{height: '3.5em', width:'200px'}}/>
                        <Column field="second" header="Second" style={{height: '3.5em', width:'65px'}}/>
                        <Column field="minute" header="Minute" style={{height: '3.5em', width:'65px'}}/>
                        <Column field="hour" header="Hour" style={{height: '3.5em', width:'65px'}}/>
                        <Column field="day" header="Day" style={{height: '3.5em', width:'80px'}}/>
                        <Column field="week" header="Week" style={{height: '3.5em', width:'90px'}}/>
                        <Column field="month" header="Month" style={{height: '3.5em', width:'100px'}}/>
                        <Column field="quarter" header="Quarter" style={{height: '3.5em', width:'100px'}}/>
                        <Column field="year" header="Year" style={{height: '3.5em', width:'100px'}}/>
                        <Column field="decade" header="Decade" style={{height: '3.5em', width:'100px'}}/>
                </DataTable>

              </div>
            </td>
          </tr>
        </table>
      </div>
    )
  }
}

export default PlutoCalculatorPennyADay;
